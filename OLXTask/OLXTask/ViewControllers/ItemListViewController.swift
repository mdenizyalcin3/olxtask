//
//  ItemListViewController.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 13/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class ItemListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var category : CategoryModel?
    var items : [Item]?
    var images : [Image]?
    fileprivate let db = SQLiteDatabase()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getItems()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getItemImages()
    }
    
    func getItems(){
        guard let category = category else {return }
        items = db.readItems(categoryId: category.id)
        self.collectionView.reloadData()
    }
    
    func getItemImages(){
        ImagesService().getImages(limit: Constant.itemRequestCount, pageLimit: 80) { (response: Result<[Image], NSError>) in
            switch response{
            case .success(let result):
                self.images = result
                self.collectionView.reloadData()
                break
            case .failure( _):
                break
            }
        }
    }
}

extension ItemListViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let collectionArray = items else {return 0}
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let collectionArray = items else {return UICollectionViewCell()}
        let item = collectionArray[indexPath.item]
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.ItemCollectionCell, for: indexPath) as? ItemCollectionCell{
            cell.itemNameLabel.text = item.name as String
            cell.layer.borderColor = UIColor.gray.cgColor
            cell.layer.borderWidth = 2
            guard var dataArray = images, images!.count >= indexPath.item else {return cell }
            let image = dataArray[indexPath.item]
            cell.setImageData(image: image)
            return cell
        }
        return UICollectionViewCell()
    }
}

extension ItemListViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = items else {return }
        let item = data[indexPath.item]
        do{
            try db.updateItem(item: item)
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: Constant.ItemDetailViewController) as?
                ItemDetailViewController{
                viewController.item = item
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }catch{
        }
    }
}

extension ItemListViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        guard let _ = items else {return CGSize.zero}
        return CGSize(width: Constant.normalItemSize, height: Constant.normalItemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
