//
//  ItemDetailViewController.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 13/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class ItemDetailViewController: UIViewController {
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemDetailImageView: UIImageView!
    @IBOutlet weak var quateLabel: UILabel!
    var item : Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getQuate()
        self.loadRandomImage()
        guard let item = item else {return }
        itemTitleLabel.text = "Item Detail of \(item.name)"
    }
    
    func loadRandomImage(){
        var imageUrl = Constant.BaseURL_Image
        guard itemDetailImageView != nil else {return }
        imageUrl += "/\(Int(itemDetailImageView.frame.size.width))/\(Int(itemDetailImageView.frame.size.height))"
        itemDetailImageView.downloaded(from: imageUrl)
    }
    
    func getQuate(){
        QuateService().getQuate { (response: Result<Quate, NSError>) in
            switch response{
            case .success(let result):
                guard self.quateLabel != nil else {return }
                self.quateLabel.text = result.content
                break
            case .failure( _):
                break
            }
        }
    }
}
