//
//  CategorySelectionViewController.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 13/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class CategorySelectionViewController: UIViewController {

    @IBOutlet weak var blockView: BlockView!
    fileprivate let db = SQLiteDatabase()
    fileprivate var sizeArray : [(width: CGFloat, height: CGFloat)] = [(1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1)]
    var categories : [CategoryModel]?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        categories = db.readCategories()!
        guard categories != nil else {return }
        self.calculateItemSize(categories: categories)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard blockView != nil else {return }
        blockView.delegate = self
    }
    
    func calculateItemSize(categories : [CategoryModel]?){
        guard var dataArray = categories, categories!.count > 1 else {return }
        self.refreshSizeArray()
        if dataArray[0].click == dataArray[1].click{
            if dataArray[0].click != dataArray.last!.click{
                dataArray.swapAt(1, 3)
                sizeArray[0] = (2, 2)
                sizeArray[3] = (2, 2)
            }
        }else{
            if dataArray[1].click == dataArray.last!.click{
                sizeArray[0] = (2, 2)
            }else{
                sizeArray[0] = (3, 1)
                sizeArray[1] = (2, 1)
            }
        }
        guard blockView != nil else {return }
        blockView.createBlockView(viewController: self, data: dataArray, sizeArray: sizeArray)
        self.getImagesRequest()
    }
    
    func refreshSizeArray(){
        sizeArray = [(1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1)]
    }
    
    func getImagesRequest(){
        ImagesService().getImages(limit: Constant.categoryRequestCount, pageLimit: 160) { (response: Result<[Image], NSError>) in
            switch response{
            case .success(let result):
                guard self.blockView != nil else {return }
                self.blockView.setImages(images: result)
                break
            case .failure( _):
                break
            }
        }
    }
}

extension CategorySelectionViewController : BlockViewDelegate{
    func didCategoryClicked(category: CategoryModel) {
        do{
            try db.updateCategory(category: category)
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: Constant.ItemListViewController) as? ItemListViewController{
                viewController.category = category
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }catch{
        }
    }
}
