//
//  WelcomeViewController.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var mostViewedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        self.loadRandomImage()
    }
    
    func setUpUI(){
        guard mostViewedLabel != nil else {return }
        mostViewedLabel.layer.shadowColor = UIColor.black.cgColor
        mostViewedLabel.layer.shadowRadius = 3.0
        mostViewedLabel.layer.shadowOpacity = 1.0
        mostViewedLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        mostViewedLabel.layer.masksToBounds = false
    }
    
    func loadRandomImage(){
        var imageUrl = Constant.BaseURL_Image
        guard categoryImageView != nil else {return }
        imageUrl = imageUrl.createImageUrl(imageView: categoryImageView)
        categoryImageView.downloaded(from: imageUrl)
    }

    @IBAction func goToListingAction(_ sender: Any) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: Constant.CategorySelectionViewController) as? CategorySelectionViewController{
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

