//
//  SQLiteDatabase+Methods.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit
import SQLite3

extension SQLiteDatabase{
    func insertCategory(category: CategoryModel) throws {
        let insertSql = "INSERT INTO Category (Id, Name, Click) VALUES (?, ?, ?)"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        guard sqlite3_bind_int(insertStatement, 1, category.id) == SQLITE_OK && sqlite3_bind_text(insertStatement, 2, category.name.utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(insertStatement, 3, category.click) == SQLITE_OK else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage)
        }
    }
    
    func insertItem(item: Item) throws {
        let insertSql = "INSERT INTO Item (Name, Click, CategoryId) VALUES (?, ?, ?)"
        let insertStatement = try prepareStatement(sql: insertSql)
        defer {
            sqlite3_finalize(insertStatement)
        }
        guard sqlite3_bind_text(insertStatement, 1, item.name.utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(insertStatement, 2, item.click) == SQLITE_OK && sqlite3_bind_int(insertStatement, 3, item.categoryId) == SQLITE_OK else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage)
        }
    }
    
    func updateCategory(category: CategoryModel) throws {
        let updateSql = "UPDATE Category SET Click = ? WHERE id = ?"
        guard let updateStatement = try? prepareStatement(sql: updateSql) else {
            return
        }
        defer {
            sqlite3_finalize(updateStatement)
        }
        guard sqlite3_bind_int(updateStatement, 1, category.click + 1) == SQLITE_OK  && sqlite3_bind_int(updateStatement, 2, category.id) == SQLITE_OK else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        guard sqlite3_step(updateStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage)
        }
    }
    
    func updateItem(item: Item) throws {
        let updateSql = "UPDATE Item SET Click = ? WHERE id = ?"
        guard let updateStatement = try? prepareStatement(sql: updateSql) else {
            return
        }
        defer {
            sqlite3_finalize(updateStatement)
        }
        guard sqlite3_bind_int(updateStatement, 1, item.click + 1) == SQLITE_OK  && sqlite3_bind_int(updateStatement, 2, item.id) == SQLITE_OK else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        guard sqlite3_step(updateStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage)
        }
    }
    
    func readItems(categoryId: Int32) -> [Item]? {
        var result : [Item] = []
        let querySql = "SELECT * FROM Item WHERE CategoryId = ?"
        guard let queryStatement = try? prepareStatement(sql: querySql) else {
            return nil
        }
        defer {
            sqlite3_finalize(queryStatement)
        }
        guard sqlite3_bind_int(queryStatement, 1, categoryId) == SQLITE_OK else {
            return nil
        }
        while (sqlite3_step(queryStatement) == SQLITE_ROW) {
            let id = sqlite3_column_int(queryStatement, 0)
            let queryResultCol1 = sqlite3_column_text(queryStatement, 1)
            let name = String(cString: queryResultCol1!) as NSString
            let click = sqlite3_column_int(queryStatement, 2)
            let categoryId = sqlite3_column_int(queryStatement, 3)
            result.append(Item(id: id, name: name, click: click, categoryId: categoryId))
        }
        return result
    }
    
    func readCategories() -> [CategoryModel]? {
        var result : [CategoryModel] = []
        let querySql = "SELECT * FROM Category ORDER BY Click DESC"
        guard let queryStatement = try? prepareStatement(sql: querySql) else {
            return nil
        }
        defer {
            sqlite3_finalize(queryStatement)
        }
        while (sqlite3_step(queryStatement) == SQLITE_ROW) {
            let id = sqlite3_column_int(queryStatement, 0)
            let queryResultCol1 = sqlite3_column_text(queryStatement, 1)
            let name = String(cString: queryResultCol1!) as NSString
            let click = sqlite3_column_int(queryStatement, 2)
            result.append(CategoryModel(id: id, name: name, click: click))
        }
        return result
    }
    
    func controlFirstTimeUser() -> Bool {
        let querySql = "SELECT * FROM Category WHERE Click != 0"
        guard let queryStatement = try? prepareStatement(sql: querySql) else {
            return false
        }
        defer {
            sqlite3_finalize(queryStatement)
        }
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            return true
        }
        return false
    }
}
