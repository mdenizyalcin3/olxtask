//
//  SQLiteDatabase.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit
import SQLite3
import os.log

enum SQLiteError: Error {
    case OpenDatabase(message: String)
    case Prepare(message: String)
    case Step(message: String)
    case Bind(message: String)
}

class SQLiteDatabase {
    fileprivate var dbPointer: OpaquePointer?
    fileprivate var dbURL: URL
    fileprivate var categoryNameArray = ["A", "B", "C", "D", "E", "F"]
    fileprivate var itemNameArray = ["G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S"]
    var errorMessage: String {
        if let errorPointer = sqlite3_errmsg(dbPointer) {
            let errorMessage = String(cString: errorPointer)
            return errorMessage
        } else {
            return "No error message provided from sqlite."
        }
    }
    
    let oslog = OSLog(subsystem: "mehmetdenizyalcin", category: "SQLiteDatabase")
    
    deinit {
        sqlite3_close(dbPointer)
    }
    
    init() {
        do {
            do {
                dbURL = try FileManager.default
                    .url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    .appendingPathComponent("integration.db")
            } catch {
                dbURL = URL(fileURLWithPath: "")
                return
            }
            try openDB()
            try createTables(table: CategoryModel.self)
            try createTables(table: Item.self)
            if self.readCategories()?.count == 0{
                for (index, category) in categoryNameArray.enumerated(){
                    do {
                        try self.insertCategory(category: CategoryModel(id: Int32(index), name: category as NSString, click: 0))
                        for (_, item) in itemNameArray.enumerated(){
                            do{
                                try self.insertItem(item: Item(id: 0, name: item as NSString, click: 0, categoryId: Int32(index)))
                            } catch {
                                os_log("Some error occurred while inserting item.")
                            }
                        }
                    } catch {
                         os_log("Some error occurred while inserting category.")
                    }
                }
            }
        } catch {
            os_log("Some error occurred. Returning.")
            return
        }
    }
    
    func openDB() throws {
        if sqlite3_open(dbURL.path, &dbPointer) != SQLITE_OK { // error mostly because of corrupt database
            os_log("error opening database at %s", log: oslog, type: .error, dbURL.absoluteString)
            //            deleteDB(dbURL: dbURL)
            defer {
                if dbPointer != nil {
                    sqlite3_close(dbPointer)
                }
            }
            throw SQLiteError.OpenDatabase(message: errorMessage)
        }
    }
    
    func deleteDB() {
        do {
            self.dbURL = try FileManager.default
                .url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("integration.db")
        } catch {
            self.dbURL = URL(fileURLWithPath: "")
            return
        }
        os_log("removing db", log: oslog)
        do {
            try FileManager.default.removeItem(at: dbURL)
        } catch {
            os_log("exception while removing db %s", log: oslog, error.localizedDescription)
        }

    }
    
    func createTables(table: SQLTable.Type) throws {
        let createTableStatement = try prepareStatement(sql: table.createStatement)
        defer {
            sqlite3_finalize(createTableStatement)
        }
        guard sqlite3_step(createTableStatement) == SQLITE_DONE else {
            throw SQLiteError.Step(message: errorMessage)
        }
    }
}

extension SQLiteDatabase {
    func prepareStatement(sql: String) throws -> OpaquePointer? {
        var statement: OpaquePointer? = nil
        guard sqlite3_prepare_v2(dbPointer, sql, -1, &statement, nil) == SQLITE_OK else {
            throw SQLiteError.Prepare(message: errorMessage)
        }
        return statement
    }
}
