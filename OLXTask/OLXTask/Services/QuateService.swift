//
//  QuateService.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 14/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class QuateService {
    
    func getQuate(complition:@escaping (Result<Quate, NSError>) -> Void){
        ConnectionManager.sharedInstance.request(request: BaseRequest(method: .get, requestType: .getQuate)) { (response: Quate?, error) in
            guard error == nil else {
                complition(.failure(error!))
                return
            }
            if let quate = response {
                complition(.success(quate))
            }else{
                complition(.failure(NSError(domain: "quato handlending error", code: 401, userInfo: nil)))
            }  
        }
    }
}
