//
//  Result.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 14/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}

