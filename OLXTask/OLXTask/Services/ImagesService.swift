//
//  ImagesServices.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 14/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class ImagesService {
    
    func getImages(limit : String, pageLimit : Int, complition:@escaping (Result<[Image], NSError>) -> Void){
        let request = BaseRequest(method: .get, requestType: .getImages)
        let number = Int.random(in: 0 ... pageLimit)
        request.parameters[Constant.page] = "\(number)"
        request.parameters[Constant.limit] = limit
        ConnectionManager.sharedInstance.request(request: request) { (response: [Image]?, error) in
            guard error == nil else {
                complition(.failure(error!))
                return
            }
            if let imageArray = response, imageArray.count > 0 {
                complition(.success(imageArray))
            }else{
                complition(.failure(NSError(domain: "empty array", code: 401, userInfo: nil)))
            }            
        }
    }
}


