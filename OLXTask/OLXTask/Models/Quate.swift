//
//  Quate.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 13/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

struct Quate : Codable {
    var content: String
    var author: String
}
