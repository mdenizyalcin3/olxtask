//
//  CategoryModel.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

struct CategoryModel {
    var id: Int32
    var name: NSString
    var click: Int32
}

extension CategoryModel: SQLTable {
    static var createStatement: String {
        return """
        CREATE TABLE IF NOT EXISTS Category (Id INTEGER UNIQUE PRIMARY KEY, Name TEXT NOT NULL, Click INTEGER);
        """
    }
}
