//
//  BlockView.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

protocol BlockViewDelegate : class{
    func didCategoryClicked(category : CategoryModel)
}

class BlockView: UIView {
    
    weak var delegate : BlockViewDelegate?
    var scrollView : UIScrollView?
    fileprivate var dataArray : [CategoryModel]?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func createBlockView(viewController: UIViewController, data: [CategoryModel], sizeArray: [(CGFloat, CGFloat)] ){
        self.refreshView()
        dataArray = data
        scrollView = UIScrollView.init(frame: CGRect(x: 10, y: 0, width: Constant.blockViewWidth, height: Constant.screenHeight - 200))
        var currentWidth : CGFloat = 0
        var currentHeight : CGFloat = 0
        var firstRowHeight : CGFloat = 0
        var lastItemY : CGFloat = 0
        for (index, item) in data.enumerated(){
            guard sizeArray.count > index else {
                return
            }
            let size = sizeArray[index]
            let imageView = UIImageView(frame: CGRect(x: currentWidth, y: currentHeight, width: Constant.normalItemSize * size.0, height: Constant.normalItemSize * size.1))
            imageView.tag = Constant.blockViewImageViewsTag + index
            scrollView?.addSubview(imageView)
            
            let button = UIButton(frame: CGRect(x: currentWidth, y: currentHeight, width: Constant.normalItemSize * size.0, height: Constant.normalItemSize * size.1))
            button.layer.borderColor = UIColor.gray.cgColor
            button.layer.borderWidth = 2
            button.setTitleColor(.white, for: .normal)
            button.setTitle(item.name as String, for: .normal)
            if let label = button.titleLabel{
                label.layer.shadowColor = UIColor.black.cgColor
                label.layer.shadowRadius = 3.0
                label.layer.shadowOpacity = 1.0
                label.layer.shadowOffset = CGSize(width: 0, height: 0)
                label.layer.masksToBounds = false
            }
            button.addTarget(self, action: #selector(categoryClicked(button:)), for: .touchUpInside)
            button.tag = Constant.blockViewButtonsTag + index
            scrollView?.addSubview(button)
            
            if currentWidth == 0 {
                firstRowHeight = button.frame.origin.y + button.frame.size.height
            }
            currentWidth += button.frame.size.width
            if currentWidth == Constant.blockViewWidth{
                lastItemY = button.frame.origin.y + button.frame.size.height
                currentHeight += button.frame.size.height
                if firstRowHeight > currentHeight{
                    currentWidth -= button.frame.size.width
                }else{
                    currentWidth = 0
                }
            }
        }
        scrollView?.contentSize = CGSize(width: Constant.blockViewWidth, height: currentHeight)
        guard let scrollView = scrollView else {return }
        self.addSubview(scrollView)
    }
    
    func setImages(images : [Image]){
        guard let scrollView = scrollView else {return }
        for (index, image) in images.enumerated(){
            for (_, view) in scrollView.subviews.enumerated(){
                if view.tag == (Constant.blockViewImageViewsTag + index){
                    if let imageView = view as? UIImageView{
                        var imageUrl : String = image.download_url
                        imageUrl = imageUrl.clearSizeString()
                        imageUrl = imageUrl.createImageUrl(imageView: imageView)
                        imageView.downloaded(from: imageUrl)
                    }
                    //(view as? UIImageView)?.downloaded(from: image.download_url, contentMode: .scaleToFill)
                    break
                }
            }
        }
    }
    
    @objc private func categoryClicked(button : UIButton){
        guard let data = dataArray else {return }
        let category = data[button.tag - Constant.blockViewButtonsTag]
        self.delegate?.didCategoryClicked(category: category)
    }
    
    func refreshView(){
        self.subviews.forEach({ $0.removeFromSuperview() })
    }
}
