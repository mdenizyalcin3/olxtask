#  OLXTASK

##  Version

1.0

## Build and Runtime Requirements

* Xcode 8.0 or later
* iOS 10.0 or later

##  About OLXTASK

Sqlite DB integration with Swift iOS app: A swift project with CRUD methods implemented in sqlite. Database keeps categories' and items' click action and create a custom view to show clasification category depends on user's action.

### Structure
* SQLiteDatabase.swift - File contains methods to initialize, open, delete database and create tables
* SQLiteDatabase+Methods - File consisting of CRUD methods
* CategoryModel.swift - A model created to store a Category/row
* Item.swift - A model created to store a Item/row

* ConnectionManager.swift - File contains URL request method and creation. It maps generic response with given class
* BaseRequest.swift - File contains request initialize and all request path in project
* ImagesService.swift - Contains method to get random images 
* QuateService.swift - Contains method to get a random quate 
* Result.swift - It has enum to use success and failure case in services
* Image.swift - A model created to map network response of image list
* Quate.swift - A model created to map network response of random quate

* Constant.swift - File contains all constant parameter which are used in project
* URL.swift - File contains methods which are added for URL class
* SQLTabel.swift - File contains protocol that used for creating tabel with statement
* UIImageView.swift - File contains methods which are added for UIImageView class
* String.swift - File contains methods which are added for String class

* BlockView - It contains custom view that change every user's click depends on click count of categories. It has dynamic view and shows resized catagory to fill entire block

* WelcomeViewController - Class to show non-first user. It contains random image and button to present category list
* CategorySelectionViewController - Class to show custom view to all user. It contains classified categories, which are retrieve from database, custom block view and take action from user, this action opens item list. It also updates click count of selected category
* ItemListViewController - Class to show list of items depends on category. Datas are retrieve from SQLite databse. List takes action from user, this action opens item list. It also updates click count of selected item
* ItemDetailViewController - Class to show random image and random quate

* ItemCollectionCell - Files contains UILabel and UIImageView, it is used in item list
