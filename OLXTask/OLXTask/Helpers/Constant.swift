//
//  Constant.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class Constant{
    static let BaseURL_Quate = "https://api.quotable.io"
    static let BaseURL_Image = "https://picsum.photos"
    static let screenWidth = UIScreen.main.bounds.size.width
    static let screenHeight = UIScreen.main.bounds.size.height
    static let blockViewWidth = screenWidth - 20
    static let normalItemSize = (screenWidth - 20) / 3
    static let blockViewButtonsTag = 200
    static let blockViewImageViewsTag = 200
    
    //Requests
    static let categoryRequestCount = "6"
    static let itemRequestCount = "12"
    static let page = "page"
    static let limit = "limit"
    
    //ViewControllers
    static let CategorySelectionViewController = "CategorySelectionViewController"
    static let ItemListViewController = "ItemListViewController"
    static let ItemDetailViewController = "ItemDetailViewController"
    
    //Cells
    static let ItemCollectionCell = "ItemCollectionCell"
}
