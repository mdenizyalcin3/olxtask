//
//  SQLTable.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

protocol SQLTable {
    static var createStatement: String { get }
}
