//
//  String.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 14/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

extension String{
    
    mutating func createImageUrl(imageView : UIImageView) -> String{
        self += "/\(Int(imageView.frame.size.width))/\(Int(imageView.frame.size.height))"
        return self
    }
    
    mutating func clearSizeString() -> String{
        var array = self.components(separatedBy: "/")
        if array.count > 4{
            array.remove(at: array.count - 1)
            array.remove(at: array.count - 1)
        }
        return array.joined(separator: "/")
    }

}
