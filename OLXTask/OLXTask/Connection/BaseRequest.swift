//
//  BaseRequest.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class BaseRequest  {
    var BASEURL : String = Constant.BaseURL_Image
    var PATH : String = ""
    var METHOD : String = ""
    var parameters : [String : String] = [:]
    
    enum METHOD : String {
        case get = "GET"
        case post = "POST"
    }
    
    enum RequestType {
        case getOneImage
        case getImages
        case getQuate
        
        func values() -> (baseURL: String, path: String) {
            switch self {
            case .getOneImage:
                return (Constant.BaseURL_Image, "/id")
            case .getImages:
                return (Constant.BaseURL_Image, "/v2/list")
            case .getQuate:
                return (Constant.BaseURL_Quate, "/random")
            }
        }
    }
    
    init(method : METHOD = .get, requestType: RequestType) {
        self.BASEURL = requestType.values().baseURL
        self.PATH = requestType.values().path
        self.METHOD = method.rawValue
    }
}

