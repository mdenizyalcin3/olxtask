//
//  ConnectionManager.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class ConnectionManager: NSObject {
    
    static let sharedInstance = ConnectionManager()
    fileprivate let TIMEOUT_INTERVAL = 60.0
    
    func request<T : Codable>(request : BaseRequest, callback: @escaping (_ response : T?, _ error : NSError?) -> Void) {
        
        let requestUrlString = request.BASEURL + request.PATH
        var requestURL = URL(string: requestUrlString)
        for item in request.parameters.keys{
            requestURL = requestURL?.appending(item, value: request.parameters[item])
        }
        var urlRequest = URLRequest.init(url: requestURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: TIMEOUT_INTERVAL)
        urlRequest.httpMethod = request.METHOD
        urlRequest.setValue("application/json", forHTTPHeaderField:"Content-Type")
        
        openRequest(request: urlRequest) { (data: Data?, urlResponse: URLResponse?, requestError: Error?) in
            guard requestError == nil else {
                callback(nil, requestError as NSError?)
                return
            }
            if let responseData = data{
                do {
                    let response = try JSONDecoder().decode(T.self, from: responseData)
                    callback(response, nil)
                } catch {
                    callback(nil, NSError(domain: "can not parse response", code: 401, userInfo: nil))
                }
            }
            }.resume()
    }
    
    private func openRequest(request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionTask {
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self as? URLSessionDelegate, delegateQueue:OperationQueue.main)
        let task = session.dataTask(with: request, completionHandler:completionHandler)
        return task
    }
}
