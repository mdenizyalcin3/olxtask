//
//  ItemCollectionCell.swift
//  OLXTask
//
//  Created by Mehmet Yalcin on 13/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import UIKit

class ItemCollectionCell: UICollectionViewCell {
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    func setImageData(image : Image){
        var imageUrl : String = image.download_url
        imageUrl = imageUrl.clearSizeString()
        imageUrl = imageUrl.createImageUrl(imageView: itemImageView)
        guard itemImageView != nil else {return }
        itemImageView.downloaded(from: imageUrl)
        
        guard itemNameLabel != nil else {return }
        itemNameLabel.layer.shadowColor = UIColor.black.cgColor
        itemNameLabel.layer.shadowRadius = 3.0
        itemNameLabel.layer.shadowOpacity = 1.0
        itemNameLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        itemNameLabel.layer.masksToBounds = false
    }
}
