//
//  OLXTaskTests.swift
//  OLXTaskTests
//
//  Created by Mehmet Yalcin on 12/08/2019.
//  Copyright © 2019 Mehmet Yalcin. All rights reserved.
//

import XCTest
import SQLite3
@testable import OLXTask

enum SQLiteError: Error {
    case OpenDatabase(message: String)
    case Prepare(message: String)
    case Step(message: String)
    case Bind(message: String)
}

class OLXTaskTests: XCTestCase {
    
    var db: SQLiteDatabase = SQLiteDatabase()
    var dbPointer: OpaquePointer?
    var dbURL: URL?
    var errorMessage: String {
        if let errorPointer = sqlite3_errmsg(dbPointer) {
            let errorMessage = String(cString: errorPointer)
            return errorMessage
        } else {
            return "No error message provided from sqlite."
        }
    }
    
    override func setUp() {
        db.deleteDB()
        db = SQLiteDatabase()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // control category initilization
    func testCategoryModel(){
        // Given
        var category = CategoryModel(id: 0, name: "test", click: 0)
        // When
        category.click = 1
        // Then
        XCTAssertNotNil(category)
        XCTAssertEqual(category.id, 0)
        XCTAssertEqual(category.name, "test")
        XCTAssertEqual(category.click, 1)
    }
    
    // control item initilization
    func testItemModel(){
        // Given
        var item = Item(id: 4, name: "test", click: 3, categoryId: 0)
        // When
        item.click = 4
        // Then
        XCTAssertNotNil(item)
        XCTAssertEqual(item.id, 4)
        XCTAssertEqual(item.name, "test")
        XCTAssertEqual(item.click, 4)
        XCTAssertEqual(item.categoryId, 0)
    }

    // control read categories from Database
    func testReadCategories(){
        // Given : Here sql query created to get categories from database
        let querySql = "SELECT * FROM Category ORDER BY Click DESC"
        var result : [CategoryModel] = []
        // When
        guard let queryStatement = try? db.prepareStatement(sql: querySql) else {
            XCTFail(errorMessage)
            return
        }
        while (sqlite3_step(queryStatement) == SQLITE_ROW) {
            let id = sqlite3_column_int(queryStatement, 0)
            let queryResultCol1 = sqlite3_column_text(queryStatement, 1)
            let name = String(cString: queryResultCol1!) as NSString
            let click = sqlite3_column_int(queryStatement, 2)
            result.append(CategoryModel(id: id, name: name, click: click))
        }
        // Then
        XCTAssertNotEqual(result.count, 0)
        XCTAssertEqual(result.count, 6)
    }
    
    // control insert category
    func testInsertCategory(){
        // Given : Here sql query and inserted category created
        let insertSql = "INSERT INTO Category (Id, Name, Click) VALUES (?, ?, ?)"
        let categories = db.readCategories()
        let category = CategoryModel(id: (categories?.last!.id)! + 1, name: "test", click: 0)
        // When
        guard let insertStatement = try? db.prepareStatement(sql: insertSql) else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_bind_int(insertStatement, 1, category.id) == SQLITE_OK && sqlite3_bind_text(insertStatement, 2, category.name.utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(insertStatement, 3, category.click) == SQLITE_OK else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            XCTFail(errorMessage)
            return
        }
        // Then
        XCTAssertEqual(db.readCategories()!.count, 7, "category could not inserted")
    }
    
    // control read items from Database
    func testReadItemsOfCategory(){
        // Given : Here sql query created to get items from database
        let querySql = "SELECT * FROM Item WHERE CategoryId = ?"
        var result : [Item] = []
        // When
        guard let queryStatement = try? db.prepareStatement(sql: querySql) else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_bind_int(queryStatement, 1, 0) == SQLITE_OK else {
            XCTFail(errorMessage)
            return
        }
        while (sqlite3_step(queryStatement) == SQLITE_ROW) {
            let id = sqlite3_column_int(queryStatement, 0)
            let queryResultCol1 = sqlite3_column_text(queryStatement, 1)
            let name = String(cString: queryResultCol1!) as NSString
            let click = sqlite3_column_int(queryStatement, 2)
            let categoryId = sqlite3_column_int(queryStatement, 3)
            result.append(Item(id: id, name: name, click: click, categoryId: categoryId))
        }
        // Then
        XCTAssertNotEqual(result.count, 0)
        XCTAssertEqual(result.count, 12)
    }
    
    // control insert item
    func testInsertItem(){
        // Given : Here sql query and inserted item created
        let insertSql = "INSERT INTO Item (Name, Click, CategoryId) VALUES (?, ?, ?)"
        let item = Item(id: 0, name: "test", click: 0, categoryId: 0)
        // When
        guard let insertStatement = try? db.prepareStatement(sql: insertSql) else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_bind_text(insertStatement, 1, item.name.utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(insertStatement, 2, item.click) == SQLITE_OK && sqlite3_bind_int(insertStatement, 3, item.categoryId) == SQLITE_OK else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_step(insertStatement) == SQLITE_DONE else {
            XCTFail(errorMessage)
            return
        }
        // Then
        XCTAssertEqual(db.readItems(categoryId: 0)?.count, 13, "item could not inserted")
    }
    
    // control click is saved for category
    func testUpdateCategory(){
         // Given : Here sql query and click updated category created
        let updateSql = "UPDATE Category SET Click = ? WHERE id = ?"
        let category = CategoryModel(id: 0, name: "A", click: 0)
        // When
        guard let updateStatement = try? db.prepareStatement(sql: updateSql) else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_bind_int(updateStatement, 1, category.click + 1) == SQLITE_OK  && sqlite3_bind_int(updateStatement, 2, category.id) == SQLITE_OK else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_step(updateStatement) == SQLITE_DONE else {
            XCTFail(errorMessage)
            return
        }
        // Then
        XCTAssertEqual(db.controlFirstTimeUser(), false, "category can not updated")
    }
    
    // control click is saved for item
    func testUpdateItem(){
        // Given : Here sql query and click updated item created
        let updateSql = "UPDATE Item SET Click = ? WHERE id = ?"
        let item = Item(id: 0, name: "G", click: 0, categoryId: 0)
        // When
        guard let updateStatement = try? db.prepareStatement(sql: updateSql) else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_bind_int(updateStatement, 1, item.click + 1) == SQLITE_OK  && sqlite3_bind_int(updateStatement, 2, 1) == SQLITE_OK else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_step(updateStatement) == SQLITE_DONE else {
            XCTFail(errorMessage)
            return
        }
        // Then
        let controlSql = "SELECT * FROM Item WHERE Id = ? AND CategoryId = ?"
        guard let queryStatement = try? db.prepareStatement(sql: controlSql) else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_bind_int(queryStatement, 1, 1) == SQLITE_OK && sqlite3_bind_int(queryStatement, 2, 0) == SQLITE_OK else {
            XCTFail(errorMessage)
            return
        }
        guard sqlite3_step(queryStatement) == SQLITE_ROW else {
            XCTFail(errorMessage)
            return
        }
        let click = sqlite3_column_int(queryStatement, 2)
        XCTAssertEqual(click, 1, "item can not updated")
    }
    
    // Asynchronous test: success fast, failure slow. Control random images request
    func testGetImages(){
        // Given here page limit, page size and success and error response created
        let limit = "6"
        let pageLimit = 160
        var imageArray  : [Image]?
        var errorResponse : NSError?
        let promise = expectation(description: "Response handled")
        // When
        ImagesService().getImages(limit: limit, pageLimit: pageLimit) { (response: Result<[Image], NSError>) in
            switch response{
            case .success(let result):
                imageArray = result
                promise.fulfill()
                break
            case .failure(let error):
                errorResponse = error
                break
            }
        }
        // Then
        wait(for: [promise], timeout: 5)
        XCTAssertNotNil(imageArray, "image array is nil")
        XCTAssertGreaterThan(imageArray!.count, 0, "image array is empty")
        XCTAssertNil(errorResponse, errorResponse!.description)
    }
    // Asynchronous test: success fast, failure slow. Control random quate request
    func testGetQuate(){
        // Given success and error response created
        var quate : Quate?
        var errorResponse : NSError?
        let promise = expectation(description: "Response handled")
        // When
        QuateService().getQuate { (response: Result<Quate, NSError>) in
            switch response{
            case .success(let result):
                quate = result
                promise.fulfill()
                break
            case .failure(let error):
                errorResponse = error
                break
            }
        }
        // Then
        wait(for: [promise], timeout: 5)
        XCTAssertNotNil(quate, "quate is nil")
        XCTAssertNil(errorResponse, errorResponse!.description)
    }
    
    // control appending parameter to url
    func testAppendItemToUrl(){
        // Given
        var url : URL = URL(string: "www.google.com")!
        // When
        url = url.appending("query", value: "test")
        // Then
        XCTAssertEqual(url.absoluteString, "www.google.com?query=test", "item can not updated")
    }
    
    // control downloading image
    func testDownloadImage(){
        // Given
        let url : URL = URL(string: "https://picsum.photos/100")!
        var imageData : Data?
        var errorResponse : NSError?
        let promise = expectation(description: "Response handled")
        // When
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil
                else {
                    errorResponse = error as NSError?
                    return
            }
            imageData = data
            promise.fulfill()
            }.resume()
        // Then
        wait(for: [promise], timeout: 5)
        XCTAssertNotNil(imageData, "image is nil")
        XCTAssertNil(errorResponse, errorResponse!.description)
    }
    
    // control add image view dimension to string
    func testStringHasDimesion(){
         // Given
        var url = "https://picsum.photos"
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 250))
        // When
        url = url.createImageUrl(imageView: imageView)
        // Then
        XCTAssertEqual(url, "https://picsum.photos/200/250", "url creation is wrong")
    }
    
    // control clear image dimension from url
    func testClearImageDimesion(){
        // Given
        var url = "https://picsum.photos/16/2500/4000"
        // When
        url = url.clearSizeString()
        // Then
        XCTAssertEqual(url, "https://picsum.photos/16", "url creation is wrong")
    }
    
    // control block view created with all categories has same click
    func testBlockViewCreationForSameClick(){
        // Given
        let blockView = BlockView(frame: CGRect(x: 0, y: 0, width: Constant.blockViewWidth, height: Constant.screenHeight))
        let sizeArray : [(width: CGFloat, height: CGFloat)] = [(1, 1), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1)]
        let categoryArray = db.readCategories()
        // When
        blockView.createBlockView(viewController: UIViewController(), data: categoryArray!, sizeArray: sizeArray)
        // Then
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.width, Constant.normalItemSize, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.height, Constant.normalItemSize, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.scrollView?.contentSize.height, Constant.normalItemSize * 2, "Block construction is wrong")
    }
    
    // control block view created with one category is resized
    func testBlockViewCreationForOneCategoryResized(){
        // Given
        let blockView = BlockView(frame: CGRect(x: 0, y: 0, width: Constant.blockViewWidth, height: Constant.screenHeight))
        let sizeArray : [(width: CGFloat, height: CGFloat)] = [(2, 2), (1, 1), (1, 1), (1, 1), (1, 1), (1, 1)]
        let categoryArray = db.readCategories()
        // When
        blockView.createBlockView(viewController: UIViewController(), data: categoryArray!, sizeArray: sizeArray)
        // Then
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.width, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.height, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.scrollView?.contentSize.height, Constant.normalItemSize * 3, "Block construction is wrong")
    }
    
    // control block view created with two category are resized, one is bigger
    func testBlockViewCreationForTwoCategoryResizedOneIsBigger(){
        // Given
        let blockView = BlockView(frame: CGRect(x: 0, y: 0, width: Constant.blockViewWidth, height: Constant.screenHeight))
        let sizeArray : [(width: CGFloat, height: CGFloat)] = [(3, 1), (2, 1), (1, 1), (1, 1), (1, 1), (1, 1)]
        let categoryArray = db.readCategories()
        // When
        blockView.createBlockView(viewController: UIViewController(), data: categoryArray!, sizeArray: sizeArray)
        // Then
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.width, Constant.normalItemSize * 3, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.height, Constant.normalItemSize, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 1)?.frame.size.width, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 1)?.frame.size.height, Constant.normalItemSize, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.scrollView?.contentSize.height, Constant.normalItemSize * 3, "Block construction is wrong")
    }
    
    // control block view created with two category are resized and these two are same size
    func testBlockViewCreationForTwoCategoryResizedAndSame(){
        // Given
        let blockView = BlockView(frame: CGRect(x: 0, y: 0, width: Constant.blockViewWidth, height: Constant.screenHeight))
        let sizeArray : [(width: CGFloat, height: CGFloat)] = [(2, 2), (1, 1), (1, 1), (2, 2), (1, 1), (1, 1)]
        let categoryArray = db.readCategories()
        // When
        blockView.createBlockView(viewController: UIViewController(), data: categoryArray!, sizeArray: sizeArray)
        // Then
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.width, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 0)?.frame.size.height, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 3)?.frame.size.width, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.viewWithTag(Constant.blockViewImageViewsTag + 3)?.frame.size.height, Constant.normalItemSize * 2, "Items are not created depends on size Array items")
        XCTAssertEqual(blockView.scrollView?.contentSize.height, Constant.normalItemSize * 4, "Block construction is wrong")
    }
    
    // control refresh block view
    func testRefreshBlockView(){
        // Given
        let blockView = BlockView(frame: CGRect(x: 0, y: 0, width: Constant.blockViewWidth, height: Constant.screenHeight))
        let sizeArray : [(width: CGFloat, height: CGFloat)] = [(2, 2), (1, 1), (1, 1), (2, 2), (1, 1), (1, 1)]
        let categoryArray = db.readCategories()
        // When
        blockView.createBlockView(viewController: UIViewController(), data: categoryArray!, sizeArray: sizeArray)
        blockView.refreshView()
        // Then
        XCTAssertEqual(blockView.subviews.count, 0, "Could not remove all subviews of blockview")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
